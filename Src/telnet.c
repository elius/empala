/*
 * telnet.c
 *
 *  Created on: Feb 13, 2017
 *      Author: arhi
 */


/** @addtogroup Telnet
 * @{
 */

#include "telnet.h"

#include <string.h>

#include "cmsis_os.h"

#include "lwip.h"
#include "lwip/api.h"
#include "lwip/sys.h"

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

static struct netconn *telnetListener;
static struct netbuf *telnetNetBuf;

#define IS_PRINTABLE(c) ((c) >= ' ' && (c) <= '~')

#define TELNET_BUF_SIZE 256
static char telnetCmd[TELNET_BUF_SIZE] = {'\0'};
static int telnetCmdComplete = 0;

/**
 * Build command from netconn input stream
 * return pointer to command null-terminated string if command is '\r' terminated or cmd buffer is full
 */
const char *const telnetGetCmd(const char **data, uint16_t *dataLen)
{
    uint16_t posTo = 0;
    uint16_t posFrom = 0;

    // Uncompleted command in command buffer exist
    if (telnetCmdComplete)
        telnetCmdComplete = 0;
    else
        posTo = strlen(telnetCmd);

    // get string with all printable symbols
    for(posFrom = 0; posFrom < (*dataLen); posFrom++)
    {
        if (posTo >= TELNET_BUF_SIZE-1 || (*data)[posFrom] == '\r')
        {
            if (posTo > 0)
                telnetCmdComplete = 1;
            break;
        }
        if (IS_PRINTABLE((*data)[posFrom]))
            telnetCmd[posTo++] = (*data)[posFrom];
    }
    // terminate command string
    telnetCmd[posTo] = '\0';
    // skip all not printable symbols
    for(; posFrom < (*dataLen) && ! IS_PRINTABLE((*data)[posFrom]); posFrom++);
    // shift in input buffer
    (*data) += posFrom;
    (*dataLen) -= posFrom;
    if (telnetCmdComplete)
        return telnetCmd;
    else
        return (const char *)0;
}

/**
 * TCP test thread
 */
void telnetTherad(void const *argument)
{
    volatile err_t err, recvErr;

    (void)argument;
    osDelay(1000);
    // create connection
    telnetListener = netconn_new(NETCONN_TCP);
    if (!telnetListener)
        vTaskDelete(0);
    // bind for listen
    err = netconn_bind(telnetListener, IP_ADDR_ANY, 23);
    if (err != ERR_OK)
    {
        netconn_delete(telnetListener);
        vTaskDelete(0);
    }
    // Start listening
    netconn_listen(telnetListener);
    for (;;)
    {
        struct netconn *newConn;
        // Accept new connection
        err_t xErr = netconn_accept(telnetListener, &newConn);
        if (xErr != ERR_OK)
        {
            continue;
        }
        // Output greeting
        netconn_write(newConn, CLI_GREETING, strlen(CLI_GREETING),
                NETCONN_NOFLAG);
        // Receive all data from new connection and mirror it back
        while ((recvErr = netconn_recv(newConn, &telnetNetBuf)) == ERR_OK)
        {
            void *data;
            const char *cmd;
            uint16_t dataLen;
            do
            {
                // get data from buffer and write it back
                netbuf_data(telnetNetBuf, &data, &dataLen);
                do
                {
                    cmd = telnetGetCmd((const char**) &data, &dataLen);
                    if (cmd)
                    {
                        // command echo
                        /*netconn_write(newConn, cmd, strlen(cmd), NETCONN_COPY);
                        netconn_write(newConn, "\r\n", 2, NETCONN_COPY);*/
                        // Process command here
                        char *buf = FreeRTOS_CLIGetOutputBuffer();
                        BaseType_t res;
                        do
                        {
                            res = FreeRTOS_CLIProcessCommand(cmd, buf,
                                    configCOMMAND_INT_MAX_OUTPUT_SIZE);
                            netconn_write(newConn, buf, strlen(buf), NETCONN_COPY);
                        } while (res != pdFALSE);
                    }
                } while (dataLen);
                // data can be in multiple buffers
            } while (netbuf_next(telnetNetBuf) >= 0);
            // delete current buffer
            netbuf_delete(telnetNetBuf);
            if(cmd)
                netconn_write(newConn, CLI_GREETING, strlen(CLI_GREETING),
                    NETCONN_NOFLAG);
        }
        // close and delete data connection
        netconn_close(newConn);
        netconn_delete(newConn);
    }
    vTaskDelete(0);
}

/**
 * Initialize telnet thread
 */
void telnetInit(void)
{
    osThreadDef(telnetThr, telnetTherad, osPriorityNormal, 0, 512);
    osThreadCreate(osThread(telnetThr), NULL);
}

/**
 * @}
 */
