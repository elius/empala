/*
 * \file uartio.c
 * \date 06-January-2017
 * \author arhi
 * \brief This file contains functions for CLI IO.
 */

/** @addtogroup uartIO
  * @{
  */

#include "main.h"

#include "uartio.h"

#ifdef USE_SERIAL_CLI

#include <string.h>

#include "cmsis_os.h"

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

/// \brief DMI input buffer size
#define CLI_IO_IN_MAX_SIZE 128
/// \brief DMI output buffer size
#define CLI_IO_OUT_MAX_SIZE 128
/// \brief Command buffer size
#define CLI_CMD_MAX_SIZE 64

/// \brief DMI input cyclic buffer
const char serialInDMIBuf[CLI_IO_IN_MAX_SIZE];
/// \brief DMI output cyclic buffer
char serialOutDMIBuf[CLI_IO_OUT_MAX_SIZE];

/// \brief Constant pointer to the next byte after #serialInDMIBuf array
const char * const ptrInDMIBufEnd = serialInDMIBuf + CLI_IO_IN_MAX_SIZE;
/// \brief Constant pointer to the next byte after #serialOutDMIBuf array
const char * const ptrOutDMIBufEnd = serialOutDMIBuf + CLI_IO_OUT_MAX_SIZE;
/// \brief Command buffer
char serialCmd[CLI_CMD_MAX_SIZE];
/// \brief Pointer to the first unread symbol in #serialInDMIBuf
const char *ptrInMsgStart;
/// \brief Pointer to the symbol after last unread symbol in #serialInDMIBuf
const char *ptrInMsgEnd;
/// \brief Pointer to the char after last symbol in #serialOutDMIBuf
char *ptrOutMsgEnd;

/// \brief Command length in #serialCmd
int cmdLen;
/// \brief Enable inputs echo.
int serialEchoMode = 1;
/// \brief Pointer to HAL handler of UART.
UART_HandleTypeDef *hCmdUart = 0;

/**
 * Get data from DMI input buffer.
 * \param[in] buf Pointer to array of char to store data
 * \param[in] bufSize Size of buf array
 * \return Count of successfully copied chars
 */
int serialGetData(char *buf, int bufSize);

/**
 * Try to get command from input stream.
 * \return Pointer to internal buffer with zero terminated command string.
 */
const char *const serialGetCmd();

/**
 * Queue data to output stream.
 * If output buffer overflowed - automatically flush buffer to DMA buffer.
 * \param[in] buf Pointer to array of char with data
 * \param[in] bufSize Size of buf array
 * \return Count of successfully queued chars
 */
int serialQueueData(const char *buf, int bufSize);

/**
 * Put data to output stream immediately.
 * If DMA operation already pending - wait until finish.
 * \param[in] buf Pointer to array of char with data
 * \param[in] bufSize Size of buf array
 * \return Count of successfully putted chars
 */
int serialPutData(const char *buf, int bufSize);

/**
 * Flush internal buffer to output stream immediately.
 * If DMA operation already pending - wait until finish.
 */
void serialOutFlush();

/**
 * Send clear screen sequence with serialQueueData function.
 */
void serialCls();

/**
 * Send greeting sequence with serialQueueData function.
 */
void serialGreeting();

/**
 * Send EOL sequence with serialQueueData function.
 */
void serialEol();

/**
 * Serial CLI task function
 */
void startSerialCliTask(void const * argument);

void serialInit(UART_HandleTypeDef *huart)
{
    ptrInMsgStart = serialInDMIBuf;
    ptrInMsgEnd = ptrInMsgStart;
    ptrOutMsgEnd = serialOutDMIBuf;
    cmdLen = 0;
    hCmdUart = huart;
    HAL_UART_Receive_DMA(hCmdUart, (uint8_t*) serialInDMIBuf, CLI_IO_IN_MAX_SIZE);

    osThreadDef(serialThr, startSerialCliTask, osPriorityNormal, 0, 384);
    osThreadCreate(osThread(serialThr), NULL);
}

int serialGetData(char *buf, int bufSize)
{
    // check parameters
    if (!buf || bufSize <= 0)
        return -1;
    // get end index from DMA counter
    ptrInMsgEnd = serialInDMIBuf
            + (CLI_IO_IN_MAX_SIZE - hCmdUart->hdmarx->Instance->NDTR);
    int len = 0;
    // unread data exist
    if (ptrInMsgStart != ptrInMsgEnd)
    {
        for (int i = 0; i < bufSize && ptrInMsgStart != ptrInMsgEnd; i++, len++)
        {
            buf[i] = *(ptrInMsgStart++);
            if (ptrInMsgStart >= ptrInDMIBufEnd)
                ptrInMsgStart = serialInDMIBuf;
        }
    }
    return len;
}

const char *const serialGetCmd()
{
    int cmdRdy = 0;
    if (!hCmdUart)
        return (const char *const)0;
    // get end index from DMA counter
    ptrInMsgEnd = ptrInDMIBufEnd - hCmdUart->hdmarx->Instance->NDTR;
    // unread data exist
    if (ptrInMsgStart != ptrInMsgEnd)
    {
        int cmdLenBefore = cmdLen;
        for (int i = cmdLen;
                i < CLI_CMD_MAX_SIZE && ptrInMsgStart != ptrInMsgEnd && !cmdRdy;
                i++, cmdLen++)
        {
            serialCmd[i] = *(ptrInMsgStart++);
            if (serialCmd[i] == '\r')
                cmdRdy = 1;
            if (ptrInMsgStart >= ptrInDMIBufEnd)
                ptrInMsgStart = serialInDMIBuf;
        }
        if (serialEchoMode)
            serialQueueData(serialCmd + cmdLenBefore, cmdLen - cmdLenBefore);
        if (cmdLen == CLI_CMD_MAX_SIZE && !cmdRdy)
        {
            // Internal command buffer overflowed
            cmdLen = 0;
            cmdRdy = 0;
            serialPutData("\a-OVF-", 6);
        }
    }
    if (cmdRdy)
    {
        serialCmd[cmdLen - 1] = '\0';
        cmdLen = 0;
        cmdRdy = 0;
        return serialCmd;
    }
    else
        return 0;
}

int serialInFlush()
{
    if (!hCmdUart)
        return -1;
    // get end index from DMA counter
    ptrInMsgEnd = ptrInDMIBufEnd - hCmdUart->hdmarx->Instance->NDTR;
    ptrInMsgStart = ptrInMsgEnd;
    return 0;
}

int serialQueueData(const char * const buf, const int bufSize)
{
    int remainSpace;
    int copied = 0;
    int toCopy = 0;
    HAL_UART_StateTypeDef uartState;
    do
    {
        // Wait for Transmission buffer ready to prevent data corruption
        do
        {
            uartState = HAL_UART_GetState(hCmdUart);
        } while ((uartState & HAL_UART_STATE_BUSY_TX) == HAL_UART_STATE_BUSY_TX);

        // Calculate free space in buffer
        remainSpace = ptrOutDMIBufEnd - ptrOutMsgEnd;

        // In not enough - use remain free space at current cycle
        toCopy = bufSize - copied;
        if (toCopy > remainSpace)
            toCopy = remainSpace;

        // Copy data to transmission buffer
        memcpy(ptrOutMsgEnd, buf + copied, toCopy);

        copied += toCopy;
        ptrOutMsgEnd += toCopy;

        // If buffer full - flush it
        if (ptrOutMsgEnd >= ptrOutDMIBufEnd)
            serialOutFlush();
    } while (copied < bufSize); // until all data copied
    return copied;
}

int serialPutData(const char *buf, int bufSize)
{
    // Put data to transmission queue
    int putted = serialQueueData(buf, bufSize);
    // Flush remaining buffer
    serialOutFlush();
    return putted;
}

void serialOutFlush()
{
    // If some data exist in transmission buffer
    if (ptrOutMsgEnd > serialOutDMIBuf)
        HAL_UART_Transmit_DMA(hCmdUart, (uint8_t*) serialOutDMIBuf,
                ptrOutMsgEnd - serialOutDMIBuf);
    // Reset transmission buffer pointer
    ptrOutMsgEnd = serialOutDMIBuf;
}

void serialCls()
{
    serialQueueData(CLI_CLS, sizeof(CLI_CLS) - 1);
}

void serialGreeting()
{
    serialQueueData(CLI_GREETING, sizeof(CLI_GREETING) - 1);
}

void serialEol()
{
    serialQueueData(CLI_EOL, sizeof(CLI_EOL) - 1);
}

void startSerialCliTask(void const * argument) {
    serialCls();
    serialGreeting();
    serialOutFlush();
    for (;;)
    {
        const char * const cmd = serialGetCmd();
        if (cmd)
        {
            serialEol();
            if (strlen(cmd) > 1) {
                char *buf = FreeRTOS_CLIGetOutputBuffer();
                BaseType_t res;
                do
                {
                    res = FreeRTOS_CLIProcessCommand(cmd, buf,
                            configCOMMAND_INT_MAX_OUTPUT_SIZE);
                    serialQueueData(buf, strlen(buf));
                } while (res != pdFALSE);
            }
            serialGreeting();
        }
        serialOutFlush();
        vTaskDelay(10);
    }
}

#endif // USE_SERIAL_CLI
/**
  * @}
  */
