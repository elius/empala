/*
 * adc_capture.c
 *
 *  Created on: Feb 16, 2017
 *      Author: arhi
 */

#include "adc_capture.h"

#include <string.h>
#include <assert.h>

#include <math.h>

#include "cmsis_os.h"

#include "lwip.h"
#include "lwip/api.h"
#include "lwip/sys.h"

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

// Hardware handlers
static ParamADCThreadInit hAdcParam = { 0, 0 };

// ADC task handler
static TaskHandle_t xADCTask = NULL;

// UDP connection parameters
static struct netconn *conn;
static ip_addr_t dstAddr = { IPADDR_BROADCAST };
static uint16_t dstPort = 1024;

// Double buffer management
volatile int adcCpltCounter = 0;
volatile int adcSecondPart = 0;

// Acquisition enable flag
volatile static uint32_t adcRun = 1;

volatile static int adcEmulation = 0;

// ADC acquisition period in microseconds
uint32_t adcAcqPeriod = 100;

// ADC DMA buffer
__IO static uint16_t adcBuf[ADC_BUF_SIZE][ADC_CHANNELS];

/**
 * Callback on DMA signal (half and complete)
 */
void ADC_DMA_Callback(void) {
	adcCpltCounter++;
	BaseType_t v;
	vTaskNotifyGiveFromISR(xADCTask, &v);
	if (v)
		taskYIELD()
}

// HAL callback function for DMA half IRQ
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
	ADC_DMA_Callback();
	adcSecondPart = 0;
}

// HAL callback function for DMA complete IRQ
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	ADC_DMA_Callback();
	adcSecondPart = 1;
}

#define RECORD_SIZE (2*ADC_CHANNELS)

#define UDP_MAX_PAYLOAD_SIZE 1458 // 1500 MTU - 42 Ethernet+IP+UDP headers
#define EMPP_HEADER_SIZE 14
#define MAX_RECORDS_PER_PACKET (unsigned int)((UDP_MAX_PAYLOAD_SIZE-EMPP_HEADER_SIZE)/RECORD_SIZE)

/**
 * Output one data record to buffer
 */
char* outPackHeader(char* outBuf, uint16_t packNumber) {
	// magic
	*(outBuf++) = 'E';
	*(outBuf++) = 'M';
	*(outBuf++) = 'P';
	*(outBuf++) = 'P';
	// period
	*((uint32_t*) outBuf) = adcAcqPeriod;
	outBuf += sizeof(uint32_t);
	// params
	*((uint16_t*) outBuf) = ADC_CHANNELS;
	outBuf += sizeof(uint16_t);
	// number
	*((uint16_t*) outBuf) = packNumber;
	outBuf += sizeof(uint16_t);
	// counts
	*((uint16_t*) outBuf) = (uint16_t) MAX_RECORDS_PER_PACKET;
	outBuf += sizeof(uint16_t);

	return outBuf;
}

int emTick = 0;
/**
 * Output one data record to buffer
 */
char* outRecord(char* outBuf, char jammed, volatile uint16_t vADC[ADC_CHANNELS]) {
	uint16_t *buf = (uint16_t*) outBuf;
	if (jammed)
		for (int ch = 0; ch < ADC_CHANNELS; ch++)
			*(buf++) = 0xFFFF;
	else if (adcEmulation) {
		// Emulation logic
		*(buf++) = (uint16_t)roundf(
				sinf(2.0f * M_PI * emTick * 50 * adcAcqPeriod / 1e6f) * 1000.0
						+ 1000.0);
		emTick++;
		if (emTick >= 1000000/adcAcqPeriod)
			emTick = 0;
		for (int ch = 1; ch < ADC_CHANNELS; ch++)
			*(buf++) = (uint16_t) ch;
	} else
		for (int ch = 0; ch < ADC_CHANNELS; ch++)
			*(buf++) = vADC[ch];
	return (char*) buf;
}

/**
 * Reinitialize TIM5 for new acquisition period
 */
void updateADCperiod() {
	hAdcParam.htim->Instance->CNT = 0;
	hAdcParam.htim->Instance->ARR = adcAcqPeriod - 1;
}

/**
 *  Main ADC acquisition thread
 */
void adcThread(void const * argument) {
	osDelay(100);
	// create connection
	conn = netconn_new(NETCONN_UDP);
	if (!conn)
		vTaskDelete(0);
	for (;;) {
		// Wait for ADC start action
		if (!adcRun)
			xTaskNotifyWait(0x00, 0xffffffff, 0x00, portMAX_DELAY);

		// Init network connection
		netconn_connect(conn, &dstAddr, dstPort);

		// Reset ADC and DMA flags
		adcCpltCounter = 0;
		adcSecondPart = 0;

		// Start DMA and timer
		updateADCperiod();
		HAL_StatusTypeDef halStatus;
		halStatus = HAL_ADC_Start_DMA(hAdcParam.hadc, (uint32_t*) adcBuf,
		ADC_CHANNELS * ADC_BUF_SIZE);
		if (halStatus != HAL_OK)
			adcRun = 0;
		halStatus = HAL_TIM_OC_Start_IT(hAdcParam.htim, TIM_CHANNEL_4);
		if (halStatus != HAL_OK)
			adcRun = 0;
		// UDP output variables
		char *udpData = 0;
		struct netbuf *netBuffer = 0;
		int bufRecord = 0;
		int bufRecordMax = 0;
		uint16_t packNumber = 0;

		// Cycle acquisition
		while (adcRun) {
			// Wait notification from DMA IRQ handlers
			if (adcCpltCounter == 0)
				xTaskNotifyWait(0x00, 0xffffffff, 0x00,
				portMAX_DELAY);

			// Break on stop command
			if (!adcRun)
				continue;

			int adcBufIndex = 0;
			int adcBufIndexMax = 0;
			adcCpltCounter--;

			// Select first or second DMA buffer half part
			if (!adcSecondPart) {
				adcBufIndex = 0;
				adcBufIndexMax = ADC_BUF_SIZE / 2;
			} else {
				adcBufIndex = ADC_BUF_SIZE / 2;
				adcBufIndexMax = ADC_BUF_SIZE;
			}

			// Print udpData to UDP buffer
			for (; adcBufIndex < adcBufIndexMax; adcBufIndex++) {
				// Detect if network buffer exist and calculate new size if not
				if (!netBuffer) {
					bufRecordMax = MAX_RECORDS_PER_PACKET;
					netBuffer = netbuf_new();
					if (!netBuffer)
						break;
					udpData = netbuf_alloc(netBuffer,
					EMPP_HEADER_SIZE + RECORD_SIZE * bufRecordMax);
					bufRecord = 0;
					// If failed to allocate packet buffer
					if (!udpData)
						break;
					udpData = outPackHeader(udpData, packNumber++);
				}
				// If failed to allocate packet buffer
				if (!udpData)
					break;

				// Put data to buffer
				udpData = outRecord(udpData, adcCpltCounter,
						adcBuf[adcBufIndex]);
				bufRecord++;

				// Flush buffer
				if (bufRecord >= bufRecordMax) {
					netconn_send(conn, netBuffer);
					netbuf_delete(netBuffer);
					netBuffer = 0;
					udpData = 0;
				}
			}
		}
		// Stop DMA
		HAL_TIM_Base_Stop(hAdcParam.htim);
		HAL_TIM_OC_Stop_IT(hAdcParam.htim, TIM_CHANNEL_4);
		HAL_ADC_Stop_DMA(hAdcParam.hadc);

		// Flush last data
		if (netBuffer) {
			if (udpData)
				netconn_send(conn, netBuffer);
			netbuf_delete(netBuffer);
			netBuffer = 0;
			udpData = 0;
		}
		// Close data connection
		netconn_disconnect(conn);
	}
}

// Start ADC function
void startADC() {
	if (!adcRun) {
		adcRun = 1;
		xTaskNotify(xADCTask, 0, eNoAction);
	}
}

// Stop ADC function
void stopADC() {
	adcRun = 0;
	xTaskNotify(xADCTask, 0, eNoAction);
}

// Change destination IP address
void adcSetDst(uint8_t *addr, uint16_t port) {
	IP4_ADDR(&dstAddr, addr[0], addr[1], addr[2], addr[3]);
	dstPort = port;
}

// Change ADC group frequency
uint32_t adcSetFreq(uint32_t freq) {
	adcAcqPeriod = (uint32_t) (1000000 / freq);
	if (adcAcqPeriod > 65536)
		adcAcqPeriod = 65536;
	if (adcAcqPeriod < 1)
		adcAcqPeriod = 1;
	return (uint32_t) (1000000 / adcAcqPeriod);
}

// Change ADC group frequency in 1 MHz base period unit
uint32_t adcSetPeriod(uint32_t period) {
	if (period > 65536)
		period = 65536;
	if (period < 1)
		period = 1;
	adcAcqPeriod = period;
	return period;
}

// Toggle ADC emulation
// return new emulation status
int adcToggleEmul() {
	if (adcEmulation)
		adcEmulation = 0;
	else
		adcEmulation = 1;
	return adcEmulation;
}

// Output current configured IP and port to string
void adcPrintStatus(char *pcWriteBuffer, size_t xWriteBufferLen) {
	(void) xWriteBufferLen;
	if (adcRun)
		sprintf(pcWriteBuffer, "ADC started.\r\n");
	else
		sprintf(pcWriteBuffer, "ADC stopped.\r\n");
	pcWriteBuffer += strlen(pcWriteBuffer);
	sprintf(pcWriteBuffer, "Destination: ");
	pcWriteBuffer += strlen(pcWriteBuffer);
	ip4addr_ntoa_r(&dstAddr, pcWriteBuffer, 16);
	pcWriteBuffer += strlen(pcWriteBuffer);
	sprintf(pcWriteBuffer, ":%hu\r\n", dstPort);
	pcWriteBuffer += strlen(pcWriteBuffer);
	uint32_t freq = (uint32_t) (1000000 / adcAcqPeriod);
	sprintf(pcWriteBuffer, "Frequency: %lu Hz\r\n", freq);
	pcWriteBuffer += strlen(pcWriteBuffer);
	sprintf(pcWriteBuffer, "Period: %lu\r\n", adcAcqPeriod);
	pcWriteBuffer += strlen(pcWriteBuffer);
}

// Initialize ADC thread
void adcTaskInit(ParamADCThreadInit* paramADC) {
	hAdcParam.hadc = paramADC->hadc;
	hAdcParam.htim = paramADC->htim;
	osThreadDef(adcThr, adcThread, osPriorityNormal, 0, 2048);
	xADCTask = osThreadCreate(osThread(adcThr), 0);
}
