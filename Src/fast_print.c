/*
 * fast_print.c
 *
 *  Created on: May 12, 2017
 *      Author: arhi
 */

#include <stdint.h>

/**
 *  Print LS digit of 8-bit integer
 */
char* fastPrint01u(char* str, uint8_t val)
{
    *(str++) = '0' + val % 10;
    return str;
}

/**
 *  Print 16-bit integer as unsigned 04 formatted string
 */
char* fastPrint04u(char* str, uint16_t val)
{
    str += 3;
    for (int i = 0; i < 4; i++)
    {
        *(str--) = '0' + val % 10;
        val /= 10;
    }
    return str + 5;
}

/**
 *  Print 16-bit integer as unsigned 05 formatted string
 */
char* fastPrint_05s(char* str, int16_t val)
{
    if (val >= 0)
        *(str++) = '+';
    else
    {
        *(str++) = '-';
        val = -val;
    }
    str += 4;
    for (int i = 0; i < 5; i++)
    {
        *(str--) = '0' + val % 10;
        val /= 10;
    }
    return str + 6;
}
