/**
 * \file cli_commands.c
 * \date 10-January-2017
 * \author arhi
 * \brief This file contains functions for CLI.
 */

/** @addtogroup CLI
 * @{
 */

#include "cli_commands.h"
#include <string.h>
#include "task.h"
#include "lwip.h"
#include "lwip/netif.h"
#include "adc_capture.h"

#define BYTEOF(i, b) (uint8_t)((i >> (8*b)) & 0xFFu)

extern struct netif gnetif;

/**
 * \brief \c hello command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunHello(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
/**
 * \brief \c ps command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunPs(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);
/**
 * \brief \c ip-link command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunIpLink(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c ip-addr command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunIpAddr(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-start command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCStart(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-stop command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCStop(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-status command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCStatus(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-setdst command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCSetDst(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-setfreq command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCSetFreq(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/**
 * \brief \c adc-setper command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCSetPeriod(char *pcWriteBuffer,
		size_t xWriteBufferLen, const char *pcCommandString);

/**
 * \brief \c adc-emulate command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunADCEmulate(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString);

/// \c hello command definition structure.
static const CLI_Command_Definition_t xRunHello = { "hello",
		"hello:\r\n Demo hello function\r\n\r\n", prvRunHello, 0 };

/// \c ps command definition structure.
static const CLI_Command_Definition_t xRunPs = { "ps",
		"ps:\r\n List OS threads\r\n\r\n", prvRunPs, 0 };

/// \c ip-link command definition structure.
static const CLI_Command_Definition_t xRunIpLink = { "ip-link",
		"ip-link:\r\n Show ip link configuration\r\n\r\n", prvRunIpLink, 0 };

/// \c ip-addr command definition structure.
static const CLI_Command_Definition_t xRunIpIf =
		{ "ip-addr", "ip-addr:\r\n Show ip interface configuration\r\n\r\n",
				prvRunIpAddr, 0 };

/// \c adc-start command definition structure.
static const CLI_Command_Definition_t xRunADCStart = { "adc-start",
		"adc-start:\r\n Start ADC operation\r\n\r\n", prvRunADCStart, 0 };

/// \c adc-stop command definition structure.
static const CLI_Command_Definition_t xRunADCStop = { "adc-stop",
		"adc-stop:\r\n Stop ADC operation\r\n\r\n", prvRunADCStop, 0 };

/// \c adc-status command definition structure.
static const CLI_Command_Definition_t xRunADCStatus = { "adc-status",
		"adc-status:\r\n ADC operation status\r\n\r\n", prvRunADCStatus, 0 };

/// \c adc-setdst command definition structure.
static const CLI_Command_Definition_t xRunADCSetDst =
		{ "adc-setdst",
				"adc-setdst <remote-ip:port>:\r\n Set destination address for data stream\r\n\r\n",
				prvRunADCSetDst, 1 };

/// \c adc-setfreq command definition structure.
static const CLI_Command_Definition_t xRunADCSetFreq = { "adc-setfreq",
		"adc-setfreq <freq>:\r\n Set ADC acquisition frequency\r\n\r\n",
		prvRunADCSetFreq, 1 };

/// \c adc-setfreq command definition structure.
static const CLI_Command_Definition_t xRunADCSetPer = { "adc-setper",
		"adc-setper <freq>:\r\n Set ADC acquisition period\r\n\r\n",
		prvRunADCSetPeriod, 1 };

/// \c adc-emulate command definition structure.
static const CLI_Command_Definition_t xRunADCEmulate = { "adc-emul",
		"adc-emul:\r\n Toggle ADC data emulation\r\n\r\n", prvRunADCEmulate, 0 };

void vRegisterCLICommands(void) {
	// Register all the command line commands defined immediately above.
	FreeRTOS_CLIRegisterCommand(&xRunHello);
	FreeRTOS_CLIRegisterCommand(&xRunPs);
	FreeRTOS_CLIRegisterCommand(&xRunIpLink);
	FreeRTOS_CLIRegisterCommand(&xRunIpIf);
	FreeRTOS_CLIRegisterCommand(&xRunADCStart);
	FreeRTOS_CLIRegisterCommand(&xRunADCStop);
	FreeRTOS_CLIRegisterCommand(&xRunADCStatus);
	FreeRTOS_CLIRegisterCommand(&xRunADCSetDst);
	FreeRTOS_CLIRegisterCommand(&xRunADCSetFreq);
	FreeRTOS_CLIRegisterCommand(&xRunADCSetPer);
	FreeRTOS_CLIRegisterCommand(&xRunADCEmulate);
}

static BaseType_t prvRunHello(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	sprintf(pcWriteBuffer, "Hello :)\r\n");
	return pdFALSE;
}

static BaseType_t prvRunPs(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;

	static TaskStatus_t *pxTaskStatusArray = 0;
	static UBaseType_t *sortOrder;
	static UBaseType_t uxArraySize = 0;
	static UBaseType_t x = 0;
	static unsigned long ulTotalRunTime;
	const int cRecordLength = 33;
	int iStrLen;

	// If data not exist
	if (pxTaskStatusArray == NULL) {
		taskENTER_CRITICAL();
		/* Take a snapshot of the number of tasks in case it changes while this
		 function is executing. */
		uxArraySize = uxTaskGetNumberOfTasks();
		/* Allocate an array index for each task. */
		pxTaskStatusArray = pvPortMalloc(
				uxArraySize * (sizeof(TaskStatus_t) + sizeof(UBaseType_t)));
		if (pxTaskStatusArray != NULL) {
			uxArraySize = uxTaskGetSystemState(pxTaskStatusArray, uxArraySize,
					&ulTotalRunTime);
			sortOrder = (UBaseType_t*) (pxTaskStatusArray + uxArraySize);
			// Sort threads by descending CPU utilization
			for (x = 0; x < uxArraySize; x++)
				sortOrder[x] = x;
			for (x = 0; x < uxArraySize; x++) {
				UBaseType_t maxTimeCounterIndex = x;
				for (UBaseType_t y = x + 1; y < uxArraySize; y++)
					if (pxTaskStatusArray[sortOrder[y]].xTaskNumber
							< pxTaskStatusArray[sortOrder[maxTimeCounterIndex]].xTaskNumber)
						maxTimeCounterIndex = y;
				UBaseType_t tmp = sortOrder[x];
				sortOrder[x] = sortOrder[maxTimeCounterIndex];
				sortOrder[maxTimeCounterIndex] = tmp;
			}
			ulTotalRunTime /= 100;
			if (ulTotalRunTime == 0)
				ulTotalRunTime = 1;
			x = 0;
		}
		taskEXIT_CRITICAL();
	}
	if (pxTaskStatusArray != NULL) {
		if (x == 0) {
			size_t freeMem = xPortGetFreeHeapSize();
			sprintf(pcWriteBuffer, "free: %6uB\r\n", freeMem);
			iStrLen = strlen(pcWriteBuffer);
			xWriteBufferLen -= iStrLen;
			pcWriteBuffer += iStrLen;
			/* Create a human readable table from the binary data. */
			sprintf(pcWriteBuffer, "%16s\t%c\t%s\t%s\t%s\t%s%%\r\n", "name",
					's', "pr", "st", "pid", "cpu");
			iStrLen = strlen(pcWriteBuffer);
			xWriteBufferLen -= iStrLen;
			pcWriteBuffer += iStrLen;
		}

		for (; x < uxArraySize; x++) {
			// Stop output if buffer full
			if (xWriteBufferLen < cRecordLength)
				return pdTRUE;

			UBaseType_t i = sortOrder[x];
			char cStatus;
			switch (pxTaskStatusArray[i].eCurrentState) {
			case eReady:
				cStatus = 'R';
				break;
			case eBlocked:
				cStatus = 'B';
				break;
			case eSuspended:
				cStatus = 'S';
				break;
			case eDeleted:
				cStatus = 'D';
				break;
			default: /* Should not get here, but it is included
			 to prevent static checking errors. */
				cStatus = 0x00;
				break;
			}

			/* Write the rest of the string. */
			sprintf(pcWriteBuffer, "%16s\t%c\t%u\t%u\t%u\t%3u%%\r\n",
					pxTaskStatusArray[i].pcTaskName, cStatus,
					(unsigned int) pxTaskStatusArray[i].uxCurrentPriority,
					(unsigned int) pxTaskStatusArray[i].usStackHighWaterMark,
					(unsigned int) pxTaskStatusArray[i].xTaskNumber,
					(unsigned int) (pxTaskStatusArray[i].ulRunTimeCounter
							/ ulTotalRunTime));
			iStrLen = strlen(pcWriteBuffer);
			xWriteBufferLen -= iStrLen;
			pcWriteBuffer += iStrLen;
		}

		/* Free the array again. */
		vPortFree(pxTaskStatusArray);
		pxTaskStatusArray = NULL;
	}

	return pdFALSE;
}

static BaseType_t prvRunIpLink(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	pcWriteBuffer[0] = '\0';
	uint32_t bcr, bsr, sr;
	HAL_ETH_ReadPHYRegister(&heth, PHY_BCR, &bcr);
	HAL_ETH_ReadPHYRegister(&heth, PHY_BSR, &bsr);
	HAL_ETH_ReadPHYRegister(&heth, PHY_SR, &sr);
	// Link status
	sprintf(pcWriteBuffer, "\tStatus\t: %s\r\n",
			(bsr & PHY_LINKED_STATUS) ? "connected" : "disconnected");
	pcWriteBuffer += strlen(pcWriteBuffer);
	// Autonegotiation
	sprintf(pcWriteBuffer, "\tAutoneg\t: ");
	pcWriteBuffer += strlen(pcWriteBuffer);
	if (bcr & PHY_AUTONEGOTIATION) {
		sprintf(pcWriteBuffer, "%s", "enabled, ");
		pcWriteBuffer += strlen(pcWriteBuffer);
		sprintf(pcWriteBuffer, "%sdone\r\n",
				(bsr & PHY_AUTONEGO_COMPLETE) ? "" : "not ");
	} else
		sprintf(pcWriteBuffer, "%s", "disabled\r\n");
	pcWriteBuffer += strlen(pcWriteBuffer);
	// Link speed
	sprintf(pcWriteBuffer, "\tSpeed\t: %sM\r\n",
			(sr & PHY_SPEED_STATUS) ? "10" : "100");
	pcWriteBuffer += strlen(pcWriteBuffer);
	// Duplex mode
	sprintf(pcWriteBuffer, "\tDuplex\t: %s\r\n",
			(sr & PHY_DUPLEX_STATUS) ? "full" : "half");
	pcWriteBuffer += strlen(pcWriteBuffer);
	// HW address
	sprintf(pcWriteBuffer, "\tEther\t: ");
	pcWriteBuffer += strlen(pcWriteBuffer);
	for (int i = 0; i < NETIF_MAX_HWADDR_LEN; i++) {
		sprintf(pcWriteBuffer + i * 3, "%02X:", gnetif.hwaddr[i]);
	}
	pcWriteBuffer += strlen(pcWriteBuffer) - 1;
	sprintf(pcWriteBuffer, "\r\n");
	pcWriteBuffer += strlen(pcWriteBuffer);

	return pdFALSE;
}

static BaseType_t prvRunIpAddr(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	pcWriteBuffer[0] = '\0';
	sprintf(pcWriteBuffer, "%d:\r\n\tName\t: %s\r\n", gnetif.num, gnetif.name);
	pcWriteBuffer += strlen(pcWriteBuffer);
	sprintf(pcWriteBuffer, "\tLink\t: %s\r\n",
			(gnetif.flags & NETIF_FLAG_LINK_UP) ? "up" : "down");
	pcWriteBuffer += strlen(pcWriteBuffer);
	sprintf(pcWriteBuffer, "\tIface\t: %s\r\n",
			(gnetif.flags & NETIF_FLAG_UP) ? "up" : "down");
	pcWriteBuffer += strlen(pcWriteBuffer);
	for (int j = 0; j < 3; j++) {
		u32_t tmp = 0;
		switch (j) {
		case 0:
			// Address
			sprintf(pcWriteBuffer, "\tAddr\t: ");
			tmp = (netif_ip4_addr(&gnetif))->addr;
			break;
		case 1:
			// Netmask
			sprintf(pcWriteBuffer, "\tMask\t: ");
			tmp = (netif_ip4_netmask(&gnetif))->addr;
			break;
		case 2:
			// Gateway
			sprintf(pcWriteBuffer, "\tGateway\t: ");
			tmp = (netif_ip4_gw(&gnetif))->addr;
			break;
		}
		pcWriteBuffer += strlen(pcWriteBuffer);
		for (int i = 0; i < 4; i++) {
			sprintf(pcWriteBuffer + i * 4, "%03d.", BYTEOF(tmp, i));
		}
		pcWriteBuffer += strlen(pcWriteBuffer) - 1;
		sprintf(pcWriteBuffer, "\r\n");
		pcWriteBuffer += strlen(pcWriteBuffer);
	}
	return pdFALSE;
}

static BaseType_t prvRunADCStart(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	startADC();
	sprintf(pcWriteBuffer, "ADC started.\r\n");
	return pdFALSE;
}

static BaseType_t prvRunADCStop(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	stopADC();
	sprintf(pcWriteBuffer, "ADC stopped.\r\n");
	return pdFALSE;
}

static BaseType_t prvRunADCStatus(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) pcCommandString;
	(void) xWriteBufferLen;
	adcPrintStatus(pcWriteBuffer, xWriteBufferLen);
	return pdFALSE;
}

static BaseType_t prvRunADCSetDst(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) xWriteBufferLen;
	BaseType_t paramLen;
	uint16_t addr[4] = { 256, 256, 256, 256 };
	uint16_t port = 0;
	uint8_t addr8_t[4];
	const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
	sscanf(param, "%hu.%hu.%hu.%hu:%hu", &addr[0], &addr[1], &addr[2], &addr[3],
			&port);
	for (int i = 0; i < 4; i++)
		if (addr[i] > 255) {
			sprintf(pcWriteBuffer, "Incorrect address\r\n");
			return pdFALSE;
		} else {
			addr8_t[i] = (uint8_t) addr[i];
		}
	if (!port) {
		sprintf(pcWriteBuffer, "Incorrect port\r\n");
		return pdFALSE;
	}
	adcSetDst(addr8_t, port);
	sprintf(pcWriteBuffer, "Destination set to: %hu.%hu.%hu.%hu:%hu\r\n",
			addr[0], addr[1], addr[2], addr[3], port);

	return pdFALSE;
}

static BaseType_t prvRunADCSetFreq(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) xWriteBufferLen;
	BaseType_t paramLen;
	uint32_t freq = 0;
	const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
	sscanf(param, "%lu", &freq);

	if (freq > 0) {
		freq = adcSetFreq(freq);
		sprintf(pcWriteBuffer, "ADC frequency %lu Hz\r\n", freq);

	}

	return pdFALSE;
}

static BaseType_t prvRunADCSetPeriod(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) xWriteBufferLen;
	BaseType_t paramLen;
	uint32_t period = 0;
	const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
	sscanf(param, "%lu", &period);

	if (period > 0) {
		period = adcSetPeriod(period);
		sprintf(pcWriteBuffer, "ADC period %lu\r\n", period);

	}

	return pdFALSE;
}

static BaseType_t prvRunADCEmulate(char *pcWriteBuffer, size_t xWriteBufferLen,
		const char *pcCommandString) {
	(void) xWriteBufferLen;

	if (adcToggleEmul())
		sprintf(pcWriteBuffer, "Emulation on\r\n");
	else
		sprintf(pcWriteBuffer, "Emulation off\r\n");

	return pdFALSE;
}

/**
 * @}
 */
