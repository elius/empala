/*
 * telnet.h
 *
 *  Created on: Feb 13, 2017
 *      Author: arhi
 */

/** @addtogroup Telnet
 * @{
 */

#ifndef TELNET_H_
#define TELNET_H_

#include "cli_commands.h"

void telnetInit(void);

#endif /* TELNET_H_ */

/**
 * @}
 */
