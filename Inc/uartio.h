/*
 * \file uartio.h
 * \date 06-January-2017
 * \author arhi
 * \brief This file contains function prototypes for CLI IO.
 */

/** @addtogroup uartIO
  * @{
  */

#ifndef UARTIO_H_
#define UARTIO_H_

#ifdef USE_SERIAL_CLI

#include "cli_commands.h"

#include "stm32f4xx_hal.h"

/**
 * Initialize internal structures.
 * \param[in] huart UART HAL handler pointer
 * \return 0 if initialization successful
 */
void serialInit(UART_HandleTypeDef *huart);

#endif // USE_SERIAL_CLI

#endif /* UARTIO_H_ */

/**
  * @}
  */
