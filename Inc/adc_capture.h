/*
 * adc_capture.h
 *
 *  Created on: Feb 16, 2017
 *      Author: arhi
 */

#ifndef ADC_CAPTURE_H_
#define ADC_CAPTURE_H_

#include <stdint.h>
#include "lwip.h"
#include "stm32f4xx_hal.h"

// ADC DMA buffer
#define ADC_BUF_SIZE 1000
#define ADC_CHANNELS 5

typedef struct {
	ADC_HandleTypeDef* hadc;
	TIM_HandleTypeDef* htim;
} ParamADCThreadInit;

extern void adcTaskInit(ParamADCThreadInit* paramADC);

extern void adcPrintStatus(char *pcWriteBuffer, size_t xWriteBufferLen);

extern void startADC();
extern void stopADC();

extern void adcSetDst(uint8_t *addr, uint16_t port);
extern uint32_t adcSetFreq(uint32_t freq);
// Change ADC group frequency in 1 MHz base period unit
uint32_t adcSetPeriod(uint32_t period);

extern int adcToggleEmul();

#endif /* ADC_CAPTURE_H_ */
